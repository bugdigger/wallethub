package com.WalletHub.Automation.Assignments;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.WalletHub.Automation.core.GenericFunctions;

public class Assignment1 {

	@Test(dataProvider="login credentials")
	public void executeMethod() 
	{
		GenericFunctions.executeTest_Assignment1();
	}
	
	@DataProvider(name="login credentials")
		public static Object[][] getDataProvider()
		{
			return new Object[][] 
			{
				
				{"xyz","abc'"},
				{"qwe","112"}
			};
		}
	
}


