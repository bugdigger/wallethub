package com.WalletHub.Automation.core;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.WalletHub.Automation.WebPageLocators.Facebook_Page;
import com.WalletHub.Automation.WebPageLocators.Test_insurancePage;

public class GenericFunctions {

	public static WebDriver driver;
	public static WebDriverWait wait;
	public static ConfigFileReader configFileReader;
	


	public static void executeTest_Assignment1()
	{
		openBrowser();
		configFileReader=new ConfigFileReader();
		//Open the Webpage
		driver.get(configFileReader.getFacebookUrl());

		//Enter email address
		driver.findElement(By.xpath(Facebook_Page.email_textbox)).sendKeys(configFileReader.getFBUsername());

		//Enter Password
		driver.findElement(By.xpath(Facebook_Page.password_textbox)).sendKeys(configFileReader.getFBPassword());

		//click on login button
		driver.findElement(By.xpath(Facebook_Page.login_button)).click();

		//click on Home button
		driver.findElement(By.xpath(Facebook_Page.Home_button)).click();


		//click on textarea to post status
		wait = new WebDriverWait(driver,30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Facebook_Page.Post_textbox)));
		driver.findElement(By.xpath(Facebook_Page.Post_textbox)).sendKeys(configFileReader.getPostmsg());

		//Switching to next window to click on Post button
		Set handles = driver.getWindowHandles();
		//System.out.println(handles);
		// Pass a window handle to the other window
		for(String handle1 : driver.getWindowHandles()) 
		{
			System.out.println(handle1);
			driver.switchTo().window(handle1);
		}


		//click on post button
		wait = new WebDriverWait(driver,30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Facebook_Page.Post_button)));
		driver.findElement(By.xpath(Facebook_Page.Post_button)).click();

		//to close browser
		driver.quit();

	}


	public static void executeTest_Assignment2()   {

		openBrowser();

		configFileReader=new ConfigFileReader();
		//Open the Webpage
		driver.get(configFileReader.getWalletHubUrl());

		//to navigate to  Star Ratings
		wait = new WebDriverWait(driver,30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Test_insurancePage.star_ratings)));

		//to hover on Star ratings
		WebElement starhover=driver.findElement(By.xpath(Test_insurancePage.starhover));
		
		Actions action = new Actions(driver);
		action.moveToElement(starhover).build().perform();

		//Click on 5th Star
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Test_insurancePage.Click_5th_Star)));
		driver.findElement(By.xpath(Test_insurancePage.Click_5th_Star)).click();

		//To click on 'Please select your Policy dropdown
		driver.findElement(By.xpath(Test_insurancePage.Policy_dropdown)).click();

		//To select Health from dropdown menu
		driver.findElement(By.xpath(Test_insurancePage.Select_Health)).click();



		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		//Adding Review in the text box
		driver.findElement(By.xpath(Test_insurancePage.Review_textbox)).sendKeys(configFileReader.getReviewData());

		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		//click on Submit button
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Test_insurancePage.Submit_button)));
		driver.findElement(By.xpath(Test_insurancePage.Submit_button)).click();

		//Handling popup as Bluestar are getting deselected automatically after selected Health from dropdown
		Alert simpleAlert = driver.switchTo().alert();
		String alertText = simpleAlert.getText();
		//System.out.println("Alert text is " + alertText);
		simpleAlert.accept();

		//click on Bluestars
		driver.findElement(By.xpath("//span[@id='overallRating']/a[5]")).click();

		//click on Submit button
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Test_insurancePage.Submit_button)));
		driver.findElement(By.xpath(Test_insurancePage.Submit_button)).click();

		/*try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
*/
		waitPageToLoad();
		
		//click on login tab
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Test_insurancePage.Login_tab)));
		driver.findElement(By.xpath(Test_insurancePage.Login_tab)).click();

		//enter username
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Test_insurancePage.Username)));
		driver.findElement(By.xpath(Test_insurancePage.Username)).sendKeys(configFileReader.getUsername());

		//Thread.sleep(500);

		//enter password
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Test_insurancePage.Password)));
		driver.findElement(By.xpath(Test_insurancePage.Password)).sendKeys(configFileReader.getPassword());

		//click on Login button
		driver.findElement(By.xpath(Test_insurancePage.Login_button)).click();

		//to extract data from review tab
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Test_insurancePage.Review_extract)));
		String actualreview=driver.findElement(By.xpath(Test_insurancePage.Review_extract)).getText();
		//System.out.println(actualreview);

		String expected=configFileReader.getReviewData();

		Assert.assertEquals(actualreview, expected);

		driver.quit();

	}

	public static void openBrowser ()
	{
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		System.setProperty("webdriver.chrome.driver", ".\\src\\test\\resources\\Utility\\chromedriver.exe");
		driver = new ChromeDriver(options);
		driver.manage().window().maximize();
		
		/*DesiredCapabilities capability=DesiredCapabilities.chrome();
		capability.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);*/
	}
	
	public static  void waitPageToLoad()
	{
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	}

	
	

}
