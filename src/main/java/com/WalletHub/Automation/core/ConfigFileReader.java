package com.WalletHub.Automation.core;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class ConfigFileReader {
	
	private Properties properties;
	private final String propertyFilePath= "src/test/resources/configs/Configuration.properties";
 
	
	public ConfigFileReader(){
		BufferedReader reader;
		try {
			reader = new BufferedReader(new FileReader(propertyFilePath));
			properties = new Properties();
			try {
				properties.load(reader);
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException("Configuration.properties not found at " + propertyFilePath);
		}		
	}
	
	public String getWalletHubUrl() {
		String url = properties.getProperty("url");
		if(url != null) return url;
		else throw new RuntimeException("url not specified in the Configuration.properties file.");
	}
	
	public String getFacebookUrl() {
		String url = properties.getProperty("fburl");
		if(url != null) return url;
		else throw new RuntimeException("FaceBook url not specified in the Configuration.properties file.");
	}
	
	
	public String getReviewData() {
		String reviewData=properties.getProperty("review_data");
		if(reviewData != null) return reviewData;
		else throw new RuntimeException("reviewData not specified in the Configuration.properties file.");
	}
	
	public String getUsername() {
		String Username=properties.getProperty("username");
		if(Username != null) return Username;
		else throw new RuntimeException("Username not specified in the Configuration.properties file.");
	}

	public String getPassword() {
		String Password=properties.getProperty("password");
		if(Password != null) return Password;
		else throw new RuntimeException("Password not specified in the Configuration.properties file.");
	}
	
	public String getFBUsername() {
		String Username=properties.getProperty("FBusername");
		if(Username != null) return Username;
		else throw new RuntimeException("Username not specified in the Configuration.properties file.");
	}

	public String getFBPassword() {
		String Password=properties.getProperty("FBpassword");
		if(Password != null) return Password;
		else throw new RuntimeException("Password not specified in the Configuration.properties file.");
	}
	
	public String getPostmsg() {
		String postMsg=properties.getProperty("Postmsg");
		if(postMsg != null) return postMsg;
		else throw new RuntimeException("Post message not specified in the Configuration.properties file.");
	}
}
