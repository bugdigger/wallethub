package com.WalletHub.Automation.WebPageLocators;

public class Facebook_Page {
	
	public static String email_textbox="//td/input[@type='email']";
	public static String password_textbox="//td/input[@type='password']";
	public static String login_button="//input[@value='Log In']";
	public static String Post_textbox="//div[@class='_4bl9 _42n-']/textarea";
	public static String Post_button="//div[@class='_45wg _69yt']/button";
	public static String Home_button="//div[@class='_3qcu _cy7']/a";
}
