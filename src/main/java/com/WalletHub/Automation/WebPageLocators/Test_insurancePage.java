package com.WalletHub.Automation.WebPageLocators;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class Test_insurancePage {

	public static String star_ratings="//span[@class='wh-rating rating_4_5']";
	public static String starhover="//span[@class='wh-rating rating_4_5']";
	public static String Click_5th_Star="//div[@class='wh-rating-choices-holder']/a[5]";
	public static String Policy_dropdown="//span[@class='drop-arrow']";
	public static String Select_Health="//ul[@class='drop-el']/li[2]";
	public static String Review_textbox="//textarea[@name='review']";
	public static String Submit_button="//input[@value='Submit']";
	public static String BlueStar="//span[@id='overallRating']/a[5]";
	public static String Login_tab="//div[@class='ng-animate-enabled basic-trans enter']/nav/ul/li[2]/a";
	public static String Username="//*[@name='em']";
	public static String Password="//div/input[@type='password']";
	public static String Login_button="//*[@class='btn blue touch-element-cl']";
	public static String Review_extract="//div[@class='review']";
}

