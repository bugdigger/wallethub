package com.PracticeScenarios;

import org.testng.annotations.Test;

public class MultipleTestPriorityGrouping {
	
	@Test(priority=2, groups={"Smoke"})
	public void Test1(){
	    System.out.println("Test 9 -priority=2, Smoke");
	}
	@Test(priority=4, groups={"Smoke"})
	public void Test2(){
	    System.out.println("Test 2 -priority=4, Smoke");
	}
	@Test(priority=1, groups={"Smoke"})
	public void Test3(){
	    System.out.println("Test 3 -priority=1, Smoke");
	}
	@Test(priority=2, groups={"Reg"})
	public void Test4(){
	    System.out.println("Test 4 -priority=2, Reg");
	}
	@Test(priority=3, groups={"Reg"})
	public void Test5(){
	    System.out.println("Test 5 -priority=3, Reg");
	}
	@Test(priority=4, groups={"Reg"})
	public void Test6(){
	    System.out.println("Test 6 -priority=4, Reg");
	}
	@Test
	public void Test7(){
	    System.out.println("Test 7");
	}

}
